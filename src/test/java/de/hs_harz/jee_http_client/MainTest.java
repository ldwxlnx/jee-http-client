/**
 * 
 */
package de.hs_harz.jee_http_client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.hs_harz.jee_http_client.Main.ArgumentException;

/**
 * @author K. Ludwig
 *
 */
public class MainTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	// TestManagement_SoSe2020_01_HttpClient.pdf
	// 3.1#1-12 -> durch Annotation @Test(expected = <Exception.class> 
	// kann geprueft werden, ob eine Methode eine erwartete Exception wirft.
	// Die Testmethode (hier testMainNoArgs) ist augrund des Stackunwindings
	// jedoch "abgelaufen", d.h. Code nach der Exception-ausloesenden Funktion
	// wird nicht mehr abgearbeitet.
	@Test(expected = Main.ArgumentException.class)
	public void testMainNoArgs() throws ArgumentException {
		Main.main(new String[] {}); // wirft Main.ArgumentException
		System.out.println("Never called!");
	}
	// TestManagement_SoSe2020_01_HttpClient.pdf 3.2#1
	@Test()
	public void testMainNoArgsMessage() {
		try {
			Main.main(new String[] {}); // wirft Main.ArgumentException
		} catch (ArgumentException e) {
			assertEquals("No arguments provided", e.getMessage());
			return;
		}
		fail("No exception thrown");// TestManagement_SoSe2020_01_HttpClient.pdf 3.2#3
	}
	// TestManagement_SoSe2020_01_HttpClient.pdf 3.2#4
	@Test()
	public void testMainHelpMessage() {
		try {
			Main.main(new String[] {"help"}); // wirft Main.ArgumentException
		} catch (ArgumentException e) {
			assertEquals("Usage: java -jar jee-http-client.jar help|version|get <url>", e.getMessage());
			return;
		}
		fail("No exception thrown");// TestManagement_SoSe2020_01_HttpClient.pdf 3.2#3
	}
	// TestManagement_SoSe2020_01_HttpClient.pdf 3.2#4
	@Test()
	public void testMainVersionMessage() {
		try {
			Main.main(new String[] {"version"}); // wirft Main.ArgumentException
		} catch (ArgumentException e) {
			assertEquals("jee-http-client version 1.0", e.getMessage());
			return;
		} 
		fail("No exception thrown");// TestManagement_SoSe2020_01_HttpClient.pdf 3.2#3
	}
	// TestManagement_SoSe2020_01_HttpClient.pdf 3.2#4
	@Test()
	public void testMainInvalidSingleArgument() {
		String invalidSingleArgument = "blahblah"; // benoetigen wir 2x, daher Variable
		try {
			Main.main(new String[] { invalidSingleArgument }); // wirft Main.ArgumentException
		} catch (ArgumentException e) {
			assertEquals(String.format("Unexpected argument <%s>",
					invalidSingleArgument), e.getMessage());
			return;
		}
		fail("No exception thrown");// TestManagement_SoSe2020_01_HttpClient.pdf 3.2#3
	}

}
