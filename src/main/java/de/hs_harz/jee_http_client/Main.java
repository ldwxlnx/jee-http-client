package de.hs_harz.jee_http_client;

import java.util.Arrays;

/**
 * Http client execution entry point
 * @author K. Ludwig
 *
 */
public class Main {
	public static class ArgumentException extends Exception {
		private static final long serialVersionUID = 1L;

		public ArgumentException(String msg) {
			super(msg);
		}
	}
	public static void main(String[] args) throws ArgumentException {
		// TestManagement_SoSe2020_01_HttpClient.pdf
		// 3.1#11
		if(args.length == 0) {
			throw new ArgumentException("No arguments provided"); // // TestManagement_SoSe2020_01_HttpClient.pdf 3.2#2
		}
		// >>> TestManagement_SoSe2020_01_HttpClient.pdf 3.2#5
		else if(args.length == 1) {
			switch (args[0]) {
			case "help": {
				throw new ArgumentException("Usage: java -jar jee-http-client.jar help|version|get <url>");
			}
			case "version": {
				throw new ArgumentException("jee-http-client version 1.0");
			}
			default:
				throw new ArgumentException(String.format("Unexpected argument <%s>", args[0]));
			}
		// <<< TestManagement_SoSe2020_01_HttpClient.pdf 3.2#5	
		}
		//TODO get <url>
	}
}